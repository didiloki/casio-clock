import Clock from "./Clock";
import Sport from "./Sport";
import clock from "clock";

const clockTime = new Clock();
// Update the clock every tick event
clock.addEventListener("tick", function () {
  clockTime.updateClock();
  clockTime.updateDate();
});

const sportMode = new Sport();
setInterval(() => {
  sportMode.steps();
  sportMode.batteryLevel();
  sportMode.heart();
}, 3000);
