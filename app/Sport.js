import document from "document";
import { today } from "user-activity";
import { battery } from "power";
import { me as appbit } from "appbit";
import sleep from "sleep";
import { HeartRateSensor } from "heart-rate";

class Sport {
  constructor() {
    this.stepsElem = document.getElementById("steps");
    this.batteryElem = document.getElementById("battery");
    this.sleepElem = document.getElementById("sleep");
    this.heartElem = document.getElementById("heartrate");
  }

  steps() {
    if (appbit.permissions.granted("access_activity")) {
      this.stepsElem.text = today.local.steps || 0;
    }
  }

  batteryLevel() {
    this.batteryElem.text = Math.floor(battery.chargeLevel) + "%";
  }

  sleep() {
    sleep.onchange = () => {
      this.sleepElem.text = sleep.state;
    };
  }
  heart() {
    const hrm = new HeartRateSensor();
    hrm.addEventListener("reading", () => {
      this.heartElem.text = hrm.heartRate;
      console.log(hrm.heartRate);
    });
    hrm.start();
  }
}

export default Sport;
