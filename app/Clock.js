import clock from "clock";
import { monoDigits, zeroPad } from "../common/utliz";
import document from "document";
import { preferences } from "user-settings";

class Clock {
  constructor() {
    this.timeElem = document.getElementById("time");
    this.dateElem = document.getElementById("date");
    this.secElem = document.getElementById("secs");
    clock.granularity = "seconds";
    this.day = ["Su", "Mn", "Tu", "We", "Th", "Fr", "Sa"];
    this.month = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
  }

  updateDate() {
    let today = new Date();
    this.dateElem.text = `${this.day[today.getDay()]} ${today.getDate()} ${
      this.month[today.getMonth()]
    }`;
  }

  updateClock() {
    let today = new Date();
    let hours = monoDigits(today.getHours());
    let mins = monoDigits(zeroPad(today.getMinutes()));
    let secs = monoDigits(zeroPad(today.getSeconds()));

    const twelveHour = preferences.clockDisplay === "12h";
    let amPm = "";
    if (twelveHour) {
      amPm = hours > 12 ? "." : "";
      hours = hours % 12 || 12;
    }

    this.timeElem.text = hours + ":" + mins;
    this.secElem.text = ":" + secs;
  }
}

export default Clock;
